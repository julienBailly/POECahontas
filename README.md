
# POECAHONTAS

En apesanteur le Musk du raton n'a pas d'odeur


![Logo](img/logo.png)

## Infos
Un dossier vide est présent mais non versionné pour permettre la persistence des données du container de la BDD
* /db -> DB postgresql


**Attention** la bdd doit contenir un schema 'poec'

Deux autres scripts permettent de créer les utilisateurs et leurs droits ainsi qu'un jeu de données

## Installation

Copier le fichier .env-template en .env & définir les valeurs des deux clefs avec des xXxXxXxX

Ce fichier servira à donner les variables d'env pour les containers et permettra de piloter le build en local pour le debug.

Créer les conteneurs des applications :

```bash
  docker compose build
  docker compose up
```
    
## API Reference du back

Une page HTML d'infos est disponible sur
```html
http://localhost:8080/swagger-ui/index.html
```


Un JSON contenant les données est accessible sur :

```http
http://localhost:8080/v3/api-docs
```

## Documentation

[Spring Boot](https://spring.io/projects/spring-boot)

[Spring Security](https://spring.io/projects/spring-security)

