package com.wijinacademy.back.api;

import com.wijinacademy.back.dto.ListDataWithPaginationInformation;
import com.wijinacademy.back.dto.ListDataWithoutPaginationInformation;
import com.wijinacademy.back.dto.UserDto;
import com.wijinacademy.back.mapper.UserMapper;
import com.wijinacademy.back.model.User;
import com.wijinacademy.back.service.UserService;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.*;


import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@Tag(name = "Gestion des utilisateurs")
public class UserController {

    private static final Integer ITEM_PER_PAGE = 10;
    @Autowired
    private UserService userService;

    @Autowired
    UserMapper userMapper;

    @GetMapping("/users")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public ListDataWithPaginationInformation<UserDto> fetchUsers(@RequestParam Optional<Integer> page) {
        
        Integer currentPage = page.orElse(0);
        Pageable paginateByUsername = PageRequest.of(currentPage, ITEM_PER_PAGE, Sort.by("username"));
        Page<User> pageInstance = userService.fetchUserList(paginateByUsername);
        Page<UserDto> pageInstanceDto = pageInstance.map(userMapper::mapUserToDtoUser);

        return new ListDataWithPaginationInformation<UserDto>(pageInstanceDto, currentPage);
    }

    @GetMapping("/users/search")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public ListDataWithoutPaginationInformation<UserDto> listUserByName(@RequestParam Optional<String> name){
        String nameQuery = name.orElse("");
        List<User> userList = userService.getByUsername(nameQuery).orElse(new ArrayList<User>());
        List<UserDto> userDtoList = userList.stream().map(userMapper::mapUserToDtoUser).toList();
        return new ListDataWithoutPaginationInformation<UserDto>(userDtoList);
    }

    @DeleteMapping("/users/{id}")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public void deleteUser(@PathVariable("id") String id){
        userService.deleteUser(Integer.parseInt(id));
    }

    @PostMapping("/users")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public void createUser(@RequestBody UserDto userDto){
        User user = userMapper.mapDtoUserToUser(userDto);
        userService.createUser(user);
    }

    @PutMapping("/users/{id}")
    @Operation(security = @SecurityRequirement(name = "bearerAuth"))
    public UserDto updateUser(@RequestBody UserDto userDto){
        User user = userMapper.mapDtoUserToUser(userDto);
        User userUpdated = userService.userUpdate(user);
        return userMapper.mapUserToDtoUser(userUpdated);
    }

}
