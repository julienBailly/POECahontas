package com.wijinacademy.back.api;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.wijinacademy.back.dto.CustomerDtoLight;
import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.tags.Tag;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.wijinacademy.back.dto.CustomerDto;
import com.wijinacademy.back.dto.ListDataWithPaginationInformation;
import com.wijinacademy.back.dto.ListDataWithoutPaginationInformation;
import com.wijinacademy.back.mapper.CustomerMapper;
import com.wijinacademy.back.model.Customer;
import com.wijinacademy.back.service.CustomerService;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;

@RestController
@Tag(name = "Gestion des clients")
public class CustomerController {

  private static final int ITEM_PER_PAGE = 10;

  @Autowired
  private CustomerService customerService;

  @Autowired
  CustomerMapper customerMapper;

  @GetMapping("/customers")
  @Operation(security = @SecurityRequirement(name = "bearerAuth"))
  public ListDataWithPaginationInformation<CustomerDtoLight> GetCustomers(@RequestParam Optional<Integer> page) {

    Integer currentPage = page.orElse(0);
    Pageable paginateByLastname = PageRequest.of(currentPage, ITEM_PER_PAGE, Sort.by("lastname"));
    Page<Customer> pageInstance = customerService.fetchCustomerList(paginateByLastname);
    Page<CustomerDtoLight> pageInstanceDto = pageInstance.map(customerMapper::mapCustomerToCustomerDtoLight);
    return new ListDataWithPaginationInformation<CustomerDtoLight>(pageInstanceDto, currentPage);
  }

  @GetMapping("/customers/search")
  @Operation(security = @SecurityRequirement(name = "bearerAuth"))
  public ListDataWithoutPaginationInformation<CustomerDtoLight> listCustomerByLastName(@RequestParam Optional<String> query) {
    String nameQuery = query.orElse("");
    List<Customer> CustomerList = customerService.searchByLastnameOrCompany(nameQuery).orElse(new ArrayList<Customer>());
    List<CustomerDtoLight> customerDtoList = CustomerList.stream().map(customerMapper::mapCustomerToCustomerDtoLight).toList();
    return new ListDataWithoutPaginationInformation<CustomerDtoLight>(customerDtoList);
  }

  @GetMapping("/customers/{id}")
  @Operation(security = @SecurityRequirement(name = "bearerAuth"))
  public CustomerDto getCustomerById(@PathVariable("id") Integer id) {

    return customerMapper.mapCustomerToCustomerDto(customerService.getById(id));
  }

  @DeleteMapping("/customers/{id}")
  @Operation(security = @SecurityRequirement(name = "bearerAuth"))
  public void deleteUser(@PathVariable("id") String id) {
    customerService.deleteCustomer(Integer.parseInt(id));
  }

  @PostMapping("/customers")
  @Operation(security = @SecurityRequirement(name = "bearerAuth"))
  public void createCustomer(@RequestBody CustomerDtoLight customerDto) {
    Customer customer = customerMapper.mapCustomerDtoLightToCustomer(customerDto);
    customerService.createCustomer(customer);
  }

  @PutMapping("/customers/{id}")
  @Operation(security = @SecurityRequirement(name = "bearerAuth"))
  public CustomerDto updateCustomer(@RequestBody CustomerDto customerDto) {
    Customer customer = customerMapper.mapCustomerDtoToCustomer(customerDto);
    Customer customerUpdated = customerService.customerUpdate(customer);
    return customerMapper.mapCustomerToCustomerDto(customerUpdated);
  }
}
