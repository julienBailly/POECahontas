package com.wijinacademy.back.mapper;

import com.wijinacademy.back.dto.CustomerDtoLight;
import com.wijinacademy.back.dto.OrderDto;
import com.wijinacademy.back.model.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wijinacademy.back.dto.CustomerDto;
import com.wijinacademy.back.model.Customer;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomerMapper {

  @Autowired
  private OrderMapper orderMapper;

  public Customer mapCustomerDtoToCustomer(CustomerDto customerDto) {
    Customer customer = new Customer();
    customer.setId(customerDto.getId());
    customer.setLastname(customerDto.getLastname());
    customer.setFirstname(customerDto.getFirstname());
    customer.setCompany(customerDto.getCompany());
    customer.setMail(customerDto.getMail());
    customer.setPhone(customerDto.getPhone());
    customer.setMobile(customerDto.getMobile());
    customer.setNotes(customerDto.getNotes());
    customer.setActive(customerDto.isActive());
    List<OrderDto> orderDtoList = customerDto.getOrders();
    List<Order> orderList = new ArrayList<>();
    for(OrderDto orderDto : orderDtoList){
      orderList.add(orderMapper.mapOrderDtoToOrder(orderDto));
    }
    customer.setOrders(orderList);
    return customer;

  }

  public Customer mapCustomerDtoLightToCustomer(CustomerDtoLight customerDto) {
    Customer customer = new Customer();
    customer.setId(customerDto.getId());
    customer.setLastname(customerDto.getLastname());
    customer.setFirstname(customerDto.getFirstname());
    customer.setCompany(customerDto.getCompany());
    customer.setMail(customerDto.getMail());
    customer.setPhone(customerDto.getPhone());
    customer.setMobile(customerDto.getMobile());
    customer.setNotes(customerDto.getNotes());
    customer.setActive(customerDto.isActive());
    return customer;

  }
  public CustomerDto mapCustomerToCustomerDto(Customer customer) {
    CustomerDto customerDto = new CustomerDto();
    customerDto.setId(customer.getId());
    customerDto.setLastname(customer.getLastname());
    customerDto.setFirstname(customer.getFirstname());
    customerDto.setCompany(customer.getCompany());
    customerDto.setMail(customer.getMail());
    customerDto.setPhone(customer.getPhone());
    customerDto.setMobile(customer.getMobile());
    customerDto.setNotes(customer.getNotes());
    customerDto.setActive(customer.isActive());
    List<Order> orderList = customer.getOrders();
    List<OrderDto> orderDtoList = new ArrayList<>();
    for (Order order : orderList){
        orderDtoList.add(orderMapper.mapOrderToOrderDto(order));
    }
    customerDto.setOrders(orderDtoList);
    return customerDto;
  }

  public CustomerDtoLight mapCustomerToCustomerDtoLight(Customer customer) {
    CustomerDtoLight customerDto = new CustomerDtoLight();
    customerDto.setId(customer.getId());
    customerDto.setLastname(customer.getLastname());
    customerDto.setFirstname(customer.getFirstname());
    customerDto.setCompany(customer.getCompany());
    customerDto.setMail(customer.getMail());
    customerDto.setPhone(customer.getPhone());
    customerDto.setMobile(customer.getMobile());
    customerDto.setNotes(customer.getNotes());
    customerDto.setActive(customer.isActive());
    List<Order> orderList = customer.getOrders();
    List<String> orderDtoList = new ArrayList<>();
    for (Order order : orderList){
      orderDtoList.add(String.valueOf(order.getId()));
    }
    customerDto.setOrders(orderDtoList);
    return customerDto;
  }

}
