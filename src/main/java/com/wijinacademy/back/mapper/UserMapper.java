package com.wijinacademy.back.mapper;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import com.wijinacademy.back.dto.UserDto;
import com.wijinacademy.back.model.User;
import com.wijinacademy.back.model.UserRole;

@Service
public class UserMapper {

     @Autowired
    private PasswordEncoder encoder; 

    public User mapDtoUserToUser(UserDto userDto){
        User user = new User();
        user.setId(userDto.getId());
        user.setUsername(userDto.getUsername());
        user.setPassword(encoder.encode(userDto.getPassword()));
        user.setMail(userDto.getMail());
        String userRoleDto = userDto.getGrants();
        UserRole userRole = UserRole.valueOf("ROLE_"+userRoleDto);
        user.setGrants(userRole);
        return user;
    }

    public UserDto mapUserToDtoUser(User user){
        UserDto userDto = new UserDto();
        userDto.setId(user.getId());
        userDto.setUsername(user.getUsername());
        userDto.setPassword(user.getPassword());
        userDto.setMail(user.getMail());
        UserRole userRole = user.getGrants();
        userDto.setGrants(userRole.getValue());
        return userDto;
    }

}
