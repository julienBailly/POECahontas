package com.wijinacademy.back.mapper;

import com.wijinacademy.back.dto.OrderDto;
import com.wijinacademy.back.model.Customer;
import com.wijinacademy.back.model.Order;
import com.wijinacademy.back.model.OrderStatus;
import com.wijinacademy.back.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.lang.reflect.Array;

@Service
public class OrderMapper {

    @Autowired
    private CustomerRepository customerRepository;

    public Order mapOrderDtoToOrder(OrderDto orderDto){
        Order order = new Order();
        order.setId(orderDto.getId());
        order.setLabel(orderDto.getLabel());
        order.setAdrEt(orderDto.getAdrEt());
        order.setNumberOfDays(orderDto.getNumberOfDays());
        order.setTva(orderDto.getTva());

        String orderStatus = orderDto.getStatus();
        OrderStatus orderStatusName = null;
        OrderStatus[] orderStatusValues = OrderStatus.values();
        for (OrderStatus oS : orderStatusValues){
            if (oS.getValue().equals(orderStatus)){
                orderStatusName = oS;
            }
        }

        order.setStatus(orderStatusName);
        order.setType(orderDto.getType());
        order.setNotes(orderDto.getNotes());
        // TODO : Throw err si optional plutot qu'objet null ?
        Customer customer = customerRepository.findById(orderDto.getCustomer()).orElse(new Customer());

        order.setCustomer(customer);
        return order;
    }

    public OrderDto mapOrderToOrderDto(Order order){
        OrderDto orderDto = new OrderDto();
        orderDto.setId(order.getId());
        orderDto.setLabel(order.getLabel());
        orderDto.setAdrEt(order.getAdrEt());
        orderDto.setNumberOfDays(order.getNumberOfDays());
        orderDto.setTva(order.getTva());
        orderDto.setStatus(order.getStatus().getValue());
        orderDto.setType(order.getType());
        orderDto.setNotes(order.getNotes());
        orderDto.setCustomer(order.getCustomer().getId());
        return orderDto;
    }
}
