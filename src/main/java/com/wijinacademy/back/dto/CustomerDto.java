package com.wijinacademy.back.dto;

import java.util.List;

public class CustomerDto {

  private Integer id;

  private String lastname;

  private String firstname;

  private String company;

  private String mail;

  private String phone;

  private String mobile;

  private String notes;

  private Boolean active;

  private List<OrderDto> orders;

  public CustomerDto() {
  }

  public CustomerDto(Integer id, String lastname, String firstname, String company, String mail, String phone,
      String mobile, String notes, Boolean active, List<OrderDto> orders) {

    this.id = id;
    this.lastname = lastname;
    this.firstname = firstname;
    this.company = company;
    this.mail = mail;
    this.phone = phone;
    this.mobile = mobile;
    this.notes = notes;
    this.active = active;
    this.orders = orders;

  }

  /**
   * @return Integer return the id
   */
  public Integer getId() {
    return id;
  }

  /**
   * @param id the id to set
   */
  public void setId(Integer id) {
    this.id = id;
  }

  /**
   * @return String return the lastname
   */
  public String getLastname() {
    return lastname;
  }

  /**
   * @param lastname the lastname to set
   */
  public void setLastname(String lastname) {
    this.lastname = lastname;
  }

  /**
   * @return String return the firstname
   */
  public String getFirstname() {
    return firstname;
  }

  /**
   * @param firstname the firstname to set
   */
  public void setFirstname(String firstname) {
    this.firstname = firstname;
  }

  /**
   * @return String return the company
   */
  public String getCompany() {
    return company;
  }

  /**
   * @param company the company to set
   */
  public void setCompany(String company) {
    this.company = company;
  }

  /**
   * @return String return the mail
   */
  public String getMail() {
    return mail;
  }

  /**
   * @param mail the mail to set
   */
  public void setMail(String mail) {
    this.mail = mail;
  }

  /**
   * @return String return the phone
   */
  public String getPhone() {
    return phone;
  }

  /**
   * @param phone the phone to set
   */
  public void setPhone(String phone) {
    this.phone = phone;
  }

  /**
   * @return String return the mobile
   */
  public String getMobile() {
    return mobile;
  }

  /**
   * @param mobile the mobile to set
   */
  public void setMobile(String mobile) {
    this.mobile = mobile;
  }

  /**
   * @return String return the notes
   */
  public String getNotes() {
    return notes;
  }

  /**
   * @param notes the notes to set
   */
  public void setNotes(String notes) {
    this.notes = notes;
  }

  /**
   * @return Boolean return the active
   */
  public Boolean isActive() {
    return active;
  }

  /**
   * @param active the active to set
   */
  public void setActive(Boolean active) {
    this.active = active;
  }

  public List<OrderDto> getOrders() {
    return orders;
  }

  public void setOrders(List<OrderDto> orders) {
    this.orders = orders;
  }
}
