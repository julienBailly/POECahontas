package com.wijinacademy.back.dto;

import java.util.List;
import org.springframework.data.domain.Page;

public class ListDataWithPaginationInformation <T>{

    private List<T> datas;

    private Integer nbPagesResult;

    private Integer currentPage;

    private Page<T> pageInstance;

    public ListDataWithPaginationInformation(Page<T> pageInstance, Integer currentPage) {
        this.pageInstance = pageInstance;
        this.datas = this.pageInstance.toList();
        this.nbPagesResult = this.pageInstance.getTotalPages();
        this.currentPage = currentPage;
    }

    public List<T> getDatas() {
        return datas;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }

    public Integer getNbPagesResult() {
        return nbPagesResult;
    }

    public void setNbPagesResult(Integer nbPagesResult) {
        this.nbPagesResult = nbPagesResult;
    }

    public Integer getCurrentPage() {
        return currentPage;
    }

    public void setCurrentPage(Integer currentPage) {
        this.currentPage = currentPage;
    }

}
