package com.wijinacademy.back.dto;


import java.util.List;

public class ListDataWithoutPaginationInformation<T>{

    private List<T> datas;


    public ListDataWithoutPaginationInformation(List<T> data) {
        this.datas = data;
    }

    public List<T> getDatas() {
        return datas;
    }

    public void setDatas(List<T> datas) {
        this.datas = datas;
    }

}
