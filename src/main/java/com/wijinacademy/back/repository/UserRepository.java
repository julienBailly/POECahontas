package com.wijinacademy.back.repository;

import com.wijinacademy.back.model.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
    
    Optional<List<User>> findByUsernameContainingIgnoreCase(String username);

    User findByUsername(String username);

    User findByUsernameAndPassword(String username, String password);
}
