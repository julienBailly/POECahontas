package com.wijinacademy.back.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.wijinacademy.back.model.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer, Integer> {

  Optional<List<Customer>> findByLastnameContainingIgnoreCaseOrCompanyContainingIgnoreCase(String lastname,String company);

}
