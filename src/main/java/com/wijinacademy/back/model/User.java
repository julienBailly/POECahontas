package com.wijinacademy.back.model;

import jakarta.persistence.*;

import java.io.Serializable;

@Entity
@Table(name="users", schema = "poec")
public class User implements Serializable {

    private static final long serialVersionUID = 1221366484235490734L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    @Column(
            name="username",
            nullable = false,
            length = 100
    )
    private String username;

    @Column(
            name="password",
            nullable = false,
            length = 150
    )
    private String password;

    @Column(
            name="mail",
            nullable = false,
            length = 100
    )
    private String mail;

    @Column(
            name="grants",
            nullable = false,
            length = 50
    )
    @Enumerated(EnumType.STRING)
    private UserRole grants;

    public User() {
        super();
    }

    public User(Integer id, String username, String password, String mail, UserRole grants) {
        super();
        this.id = id;
        this.username = username;
        this.password = password;
        this.mail = mail;
        this.grants = grants;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    public UserRole getGrants() {
        return grants;
    }

    public void setGrants(UserRole grants) {
        this.grants = grants;
    }

    @Override
    public String toString() {
        return "User [id=" + id + ", username=" + username + ", password=" + password + ", mail=" + mail + ", grants="
                + grants + "]";
    }

}
