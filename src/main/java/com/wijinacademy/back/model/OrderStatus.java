package com.wijinacademy.back.model;

public enum OrderStatus {
    EN_PREPARATION ("En préparation"),
    EN_PRODUCTION ("En production"),
    PRET_POUR_FRONDE ("Prêt pour lancement"),
    EN_ORBITE ("En apesanteur"),
    ;

    private String value;
    
    OrderStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

}
