package com.wijinacademy.back.model;

public enum UserRole {

    ROLE_ADMIN("ADMIN"),
    ROLE_COMMERCIAL ("COMMERCIAL"),
    ROLE_OUVRIER ("OUVRIER");

    private String value;
    UserRole(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
