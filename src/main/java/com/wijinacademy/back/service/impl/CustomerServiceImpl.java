package com.wijinacademy.back.service.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import com.wijinacademy.back.model.Customer;
import com.wijinacademy.back.repository.CustomerRepository;
import com.wijinacademy.back.service.CustomerService;

@Service
public class CustomerServiceImpl implements CustomerService {

  @Autowired
  private CustomerRepository customerRepository;

  @Override
  public Customer createCustomer(Customer customer) {
    return customerRepository.save(customer);
  }

  @Override
  public Customer customerUpdate(Customer customer) {
    return customerRepository.save(customer);
  }

  @Override
  public void deleteCustomer(Integer id) {
    customerRepository.deleteById(id);
  }

  @Override
  public List<Customer> fetchCustomerList() {
    return customerRepository.findAll();
  }

  @Override
  public Page<Customer> fetchCustomerList(Pageable pageable) {
    return customerRepository.findAll(pageable);
  }

  @Override
  public Optional<List<Customer>> searchByLastnameOrCompany(String query) {
    return customerRepository.findByLastnameContainingIgnoreCaseOrCompanyContainingIgnoreCase(query,query);
  }

  @Override
  public Customer getById(Integer id) {
    Optional<Customer> customer = customerRepository.findById(id);
    return customer.get();
  }

}
