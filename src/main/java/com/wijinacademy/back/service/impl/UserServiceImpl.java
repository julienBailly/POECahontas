package com.wijinacademy.back.service.impl;

import com.wijinacademy.back.model.User;
import com.wijinacademy.back.repository.UserRepository;
import com.wijinacademy.back.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserRepository userRepository;

    @Override
    public User createUser(User user) {
        return userRepository.save(user);
    }

    @Override
    public Optional<User> getById(Integer id) {
        return userRepository.findById(id);
    }

    @Override
    public List<User> fetchUserList() {
        return userRepository.findAll();
    }

    @Override
    public Page<User> fetchUserList(Pageable pageable) {
        return userRepository.findAll(pageable);
    }

    @Override
    public User userUpdate(User user) {
        return userRepository.save(user);
    }

    @Override
    public void deleteUser(Integer id) {
        userRepository.deleteById(id);
    }

    @Override
    public Optional<List<User>> getByUsername(String username) {
        return userRepository.findByUsernameContainingIgnoreCase(username);
    }
}
