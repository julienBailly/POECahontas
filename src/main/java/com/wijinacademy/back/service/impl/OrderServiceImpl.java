package com.wijinacademy.back.service.impl;

import com.wijinacademy.back.model.Order;
import com.wijinacademy.back.model.User;
import com.wijinacademy.back.repository.OrderRepository;
import com.wijinacademy.back.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderRepository orderRepository;

    @Override
    public Order createOrder(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public Order updateOrder(Order order) {
        return orderRepository.save(order);
    }

    @Override
    public void deleteOrder(Order order) {
        orderRepository.delete(order);
    }

    @Override
    public Optional<Order> getById(Integer id) {
        return orderRepository.findById(id);
    }

    @Override
    public List<Order> getCustomerOrders(User user) {
        return null;
    }

    @Override
    public Page<Order> getCustomerOrders(Pageable pageable) {

        return orderRepository.findAll(pageable);
    }
}
