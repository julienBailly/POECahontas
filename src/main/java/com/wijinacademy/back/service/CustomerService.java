package com.wijinacademy.back.service;

import java.util.List;
import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import com.wijinacademy.back.model.Customer;

public interface CustomerService {

  Customer createCustomer(Customer customer);

  Customer getById(Integer id);

  List<Customer> fetchCustomerList();

  Page<Customer> fetchCustomerList(Pageable pageable);

  Customer customerUpdate(Customer customer);

  void deleteCustomer(Integer id);

  Optional <List<Customer>> searchByLastnameOrCompany(String query);

}
