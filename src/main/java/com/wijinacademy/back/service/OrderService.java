package com.wijinacademy.back.service;

import com.wijinacademy.back.model.Order;
import com.wijinacademy.back.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface OrderService {
    Order createOrder (Order order);
    Order updateOrder (Order order);
    void deleteOrder (Order order);
    Optional<Order> getById (Integer id);
    List<Order> getCustomerOrders(User user);
    Page<Order> getCustomerOrders(Pageable pageable);
}
