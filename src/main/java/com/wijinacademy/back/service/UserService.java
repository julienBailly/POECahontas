package com.wijinacademy.back.service;

import com.wijinacademy.back.model.User;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

public interface UserService {

    User createUser(User user);

    Optional<User> getById(Integer id);

    List<User> fetchUserList();

    Page<User> fetchUserList(Pageable pageable);

    User userUpdate(User user);

    void deleteUser(Integer id);
    Optional<List<User>> getByUsername(String username);
}
