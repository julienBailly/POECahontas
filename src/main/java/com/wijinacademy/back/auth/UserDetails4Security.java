package com.wijinacademy.back.auth;

import java.util.ArrayList;
import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import com.wijinacademy.back.model.User;

public class UserDetails4Security implements UserDetails {

  	private User user;
	
	public UserDetails4Security(User user) {
		this.user = user;
	}

  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
		Collection<GrantedAuthority> liste =
				new ArrayList<>();
		liste.add(new SimpleGrantedAuthority(user.getGrants().toString()));
		return liste;
  }

  @Override
  public String getPassword() {
  return user.getPassword();
  }

  @Override
  public String getUsername() {
    return user.getUsername();
  }

	@Override
	public boolean isAccountNonExpired() {
		return true;
	}

	@Override
	public boolean isAccountNonLocked() {
		return true;
	}

	@Override
	public boolean isCredentialsNonExpired() {
		return true;
	}

	@Override
	public boolean isEnabled() {
		return true;
	}
  
}
