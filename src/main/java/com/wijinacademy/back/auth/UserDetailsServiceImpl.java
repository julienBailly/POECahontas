package com.wijinacademy.back.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.wijinacademy.back.model.User;
import com.wijinacademy.back.repository.UserRepository;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {
  
  @Autowired
  private UserRepository userRepo;

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    		if (null == username || username.trim().length() == 0) {
			throw new UsernameNotFoundException("Missing user with no-username " 
					+ username);
		}

		User user = userRepo.findByUsername(username);
		if (null == user) {
			throw new UsernameNotFoundException("Missing user with username " 
					+ username);
		}
		return new UserDetails4Security(user);
  }
  
}
