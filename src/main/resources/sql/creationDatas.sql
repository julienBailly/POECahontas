INSERT INTO poec.customers (lastname, firstname, company, mail, phone, mobile, notes, active) VALUES ('JONES', 'Indiana', 'Université de Chicago', 'indiana.jonas@univ-chicago.com', '0222222222', '0666666666', 'Les notes d''Indiana', true);

INSERT INTO poec.customers (lastname, firstname, company, mail, phone, mobile, notes, active) VALUES ('KENOBI', 'Obi-Wan', 'Jedis', 'obiwan.kenobi@jedis.com', '0222222222', '0666666666', 'Les notes d''Obi Wan', true);

INSERT INTO poec.customers (lastname, firstname, company, mail, phone, mobile, notes, active) VALUES ('MCCLANE', 'John', 'NYPD', 'john.mcclane@nypd.com', '0222222222', '0666666666', 'Les notes de John', false);

INSERT INTO poec.customers (lastname, firstname, company, mail, phone, mobile, notes, active) VALUES ('MCFLY', 'Marty', 'DOC', 'marty.mcfly@doc.com', NULL, NULL, 'Les notes de Marty', false);

INSERT INTO poec.orders (customer_id, label, adr_et, number_of_days, tva, status, type, notes) VALUES (1, 'Formation Java', 450.0, 5, 20, 'EN_PREPARATION', 'Forfait', 'Test');

INSERT INTO poec.orders (customer_id, label, adr_et, number_of_days, tva, status, type, notes) VALUES (1, 'Formation Spring', 450.0, 3, 20.0, 'EN_PREPARATION', 'Forfait', 'Test');

INSERT INTO poec.orders (customer_id, label, adr_et, number_of_days, tva, status, type, notes) VALUES (2, 'Formation Jedi', 1500.0, 2, 20.0, 'EN_PREPARATION', 'Forfait', 'Notes sur la formation');

INSERT INTO poec.users (username, password, mail, grants) VALUES ('toto', '$2a$12$UyxAJkYQAWKVdjnzUYhTpO2Rvn1j1/a0JY6K7YPTbiRtiPDbA/WAe', 'toto@titi.com', 'ROLE_ADMIN');

INSERT INTO poec.users (username, password, mail, grants) VALUES ('titi', '$2a$12$UyxAJkYQAWKVdjnzUYhTpO2Rvn1j1/a0JY6K7YPTbiRtiPDbA/WAe', 'toto@titi.com', 'ROLE_COMMERCIAL');

INSERT INTO poec.users (username, password, mail, grants) VALUES ('tata', '$2a$12$UyxAJkYQAWKVdjnzUYhTpO2Rvn1j1/a0JY6K7YPTbiRtiPDbA/WAe', 'toto@titi.com', 'ROLE_OUVRIER');