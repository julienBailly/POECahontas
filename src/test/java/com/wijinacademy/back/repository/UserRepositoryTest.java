package com.wijinacademy.back.repository;

import java.util.ArrayList;
import java.util.Optional;

import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import com.wijinacademy.back.model.User;
import com.wijinacademy.back.model.UserRole;

@DataJpaTest
@DisplayName("Test du Repo de l'utilisateur")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
public class UserRepositoryTest {

  @Autowired
  private UserRepository userRepository;

  private static User bob;

  @BeforeAll
  static void start() {
    bob = new User();
    bob.setPassword("$2a$12$9X/XD/eJubHc.lMBjc3wwO14aaJBN98hEVxpd0irgPkwDo9g1M0cy");
    bob.setGrants(UserRole.ROLE_ADMIN);
    bob.setUsername("bob");
    bob.setMail("bob@bob.bob");
  }

  @Order(1)
  @Test
  void getAllUsersTest() {
    this.testSizeTable(1);
  }

  private void testSizeTable(Integer expectedSize) {
    ArrayList<User> datas;
    datas = (ArrayList<User>) userRepository.findAll();
    Assertions.assertEquals(expectedSize, datas.size());
  }

  @ParameterizedTest
  @ValueSource(ints = {1})
  void getCustomerByIdTest(int id) {
    User user = userRepository.findById(id).orElse(new User());
    Assertions.assertEquals("toto", user.getUsername() );
  }

  @Order(2)
  @Test
  void createCustomerTest() {
    userRepository.save(bob);
    this.testSizeTable(2);
  }

  @Order(3)
  @Test
  void getUserByUsernameTest() {
    User clonedBob = userRepository.findByUsername("toto");
    Assertions.assertEquals("toto", clonedBob.getUsername());
  }

  @Order(4)
  @Test
  void updateUserTest() {
    User bobFromDB = userRepository.findByUsername("toto");
    bobFromDB.setPassword("1234");
    userRepository.save(bobFromDB);
    User updatedBob = userRepository.findByUsername("toto");
    Assertions.assertEquals(bobFromDB.getPassword(), updatedBob.getPassword());
  }

  @Order(5)
  @Test
  void deleteUserTest() {
    User bobFromDB = userRepository.findByUsername("toto");
    userRepository.deleteById(bobFromDB.getId());
    this.testSizeTable(0);
  }
}
