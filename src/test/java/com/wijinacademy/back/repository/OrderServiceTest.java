package com.wijinacademy.back.repository;

import com.wijinacademy.back.model.*;
import com.wijinacademy.back.model.Order;
import org.checkerframework.checker.units.qual.C;
import org.junit.jupiter.api.*;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.io.Console;
import java.util.ArrayList;
import java.util.List;

@DataJpaTest
@DisplayName("Test du Repo des commandes")
@TestMethodOrder(MethodOrderer.OrderAnnotation.class)
class OrderServiceTest {

    @Autowired
    private OrderRepository orderRepository;

    private static Order papy;

    @BeforeAll
    static void start() {
        papy = new Order();
        papy.setAdrEt(2.2);
        papy.setLabel("papy to the moooon");
        papy.setNotes("Il a toujours eu la tête dans les étoiles");
        papy.setNumberOfDays(3.0);
        papy.setTva(20.0);
        papy.setStatus(OrderStatus.EN_PRODUCTION);
    }

    @org.junit.jupiter.api.Order(1)
    @Test
    void createOrder() {
        orderRepository.save(papy);
        Assertions.assertEquals(4, orderRepository.findAll().size());
    }

    @org.junit.jupiter.api.Order(2)
    @ParameterizedTest
    @ValueSource(ints = {1})
    void getById(int id) {
        Order order = orderRepository.findById(id).orElse(new Order());
        Assertions.assertEquals("Formation Java",order.getLabel());
    }

    @org.junit.jupiter.api.Order(3)
    @Test
    void updateOrder() {
        Order papyOrder = orderRepository.findById(3).orElse(new Order());
        papyOrder.setStatus(OrderStatus.EN_ORBITE);
        orderRepository.save(papyOrder);
        Order papyUpdate = orderRepository.findById(3).orElse(new Order());
        Assertions.assertEquals(papyOrder.getStatus(), papyUpdate.getStatus());
    }

    @org.junit.jupiter.api.Order(4)
    @ParameterizedTest
    @ValueSource(ints = {1})
    void getCustomerOrders(int id) {
        List<Order> orders = orderRepository.findAll();
        List<Order> result = new ArrayList<>();
        for (Order o : orders) {
            if (o.getCustomer().getId() == id)
                result.add(o);
        }
        Assertions.assertEquals(2,result.size());
    }

    @org.junit.jupiter.api.Order(5)
    @Test
    void deleteOrder() {
        int size = orderRepository.findAll().size();
        Order papyOrder = orderRepository.findById(3).orElse(new Order());
        orderRepository.deleteById(papyOrder.getId());
        Assertions.assertEquals(size-1,orderRepository.findAll().size());
    }
}